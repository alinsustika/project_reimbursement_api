const { body } = require('express-validator')

const logger = require('../../winston-config')
const db = require('../models')

// @desc    Get All Klaim
// @route   GET /api/v1/klaim/
// @access  Public
exports.getAll = async (req, res, next) => {
    try {
        const post = await db.Klaim.findAll({
            include: [{ model: db.user, as: 'users' }],
            logging:console.log
        });

        res.status(200).json({ success: true, posts: post });
    } catch (err) {
        
        res.status(500).json({ success: false, msg: err.message });
    }
};

// @desc    Get a Post
// @route   GET /api/v1/posts/:id
// @access  Public
exports.getKlaim = async (req, res, next) => {
    try {
        console.log("REQ PARAMS", req.params);
        const { id } = req.params;
        console.log("REQ PARAMS ID", id);
        const post = await db.Klaim.findOne({
            where: { id },
            include: [
                {
                    model: db.user,
                    as: 'users',
                },
            ],
            logging:console.log
        });

        if (!post) {
            return res
                .status(404)
                .json({ success: false, msg: 'Klaim Does Not Exist' });
        }

        res.status(200).json({ success: true, post });
    } catch (err) {
        
        res.status(500).json({ success: false, msg: err.message });
    }
};

// @desc    Create A Post
// @route   POST /api/v1/posts/
// @access  Public
exports.createKlaim = async (req, res, next) => {
    try {
        const data = req.body;
        
        const post = await db.Klaim.create(data);

        res.status(200).json({ success: true, post });
    } catch (err) {
        
        res.status(500).json({ success: false, msg: err.message });
    }
};

// @desc    Edit A Post
// @route   PATCH /api/v1/posts/:id
// @access  Public
exports.editKlaim = async (req, res, next) => {
    try {
        const { id } = req.params;
        const data = req.body;
        const post = await db.Klaim.update(
            data,
            { where: { id } }
        );

        if (post[0] === 0) {
            return res.status(404).json({
                success: false,
                msg: 'Post Not Found',
            });
        }

        res.status(200).json({ success: true, msg: 'Post Updated' });
    } catch (err) {
        console.log(`${err}`.red.bold);
        res.status(500).json({ success: false, msg: `Internal Server Error` });
    }
};

// @desc    Delete A Post
// @route   DELETE /api/v1/posts/:id
// @access  Public
exports.deleteKlaim = async (req, res, next) => {
    try {
        const { id } = req.params;
        const post = await db.Klaim.destroy({ where: { id } });

        if (!post) {
            return res
                .status(404)
                .json({ success: false, msg: 'Post Not Found.' });
        }

        res.status(200).json({ success: true, msg: 'Post Deleted' });
    } catch (err) {
        console.log(`${err}`.red.bold);
        res.status(500).json({ success: false, msg: `Internal Server Error` });
    }
};