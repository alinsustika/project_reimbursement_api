const express = require('express')

const router = express.Router()

const authRoutes = require('./auth.routes')
const userRoutes = require('./users.routes')
const klaimRoutes = require('./klaim.routes');

// Auth routes
router.use('/auth', authRoutes)
// User routes
router.use('/user', userRoutes)

router.use('/klaim', klaimRoutes);

module.exports = router
