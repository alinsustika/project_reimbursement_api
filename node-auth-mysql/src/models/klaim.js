const bcrypt = require('bcrypt')

const logger = require('../../winston-config')

module.exports = (sequelize, DataTypes) => {
  const Klaim = sequelize.define('Klaim', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
        type: DataTypes.INTEGER, 
        allowNull:false,
        references: {
            model: 'users',
            key: 'id'
          }
    },
    jenisKlaim: {
        type: DataTypes.STRING, 
        allowNull:false
    },
    jumlah: {
        type:DataTypes.STRING, 
        allowNull:false
    },
    vendor: {
        type:DataTypes.STRING, 
        allowNull:false
    },
    alamat: {
        type:DataTypes.STRING, 
        allowNull:false
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull:false
    },
    deskripsi: {
        type: DataTypes.STRING, 
        allowNull:false
    },
    tanggal: {
      type: DataTypes.DATE,
      allowNull:false
    },
    foto: {
      type: DataTypes.STRING,
      allowNull: false
    },
    status:{
        type:DataTypes.STRING, 
        allowNull:false
    }
  })

  Klaim.associate = (models) => {
    Klaim.belongsTo(models.user, { foreignKey: 'userId', as: 'users' });
    
  };

  
  return Klaim
}
