const express = require('express')
const router = express.Router()
const { validate } = require('../../utils/utils')
const {
  getAll,
  getKlaim,
  editKlaim,
  deleteKlaim,
  createKlaim
} = require('../controllers/klaim.controller')

router.post('/create', createKlaim)

router.post('/edit', editKlaim)



router.get('/all', getAll);

router.get('/get/:id', getKlaim)

router.put('/update/:id', editKlaim);

router.delete('/delete', deleteKlaim);

module.exports = router
